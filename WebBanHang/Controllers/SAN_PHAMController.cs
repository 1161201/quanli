﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using System.Web.Configuration;
using MyConnect;
using PetaPoco;

namespace WebBanHang.Controllers
{
    public class SAN_PHAMController : Controller
    {
        //
        // GET: /Default1/
        public ActionResult Index(int? page)
         {
            if (page == null)
                page = 1;
            Page<CHI_TIET> dl = MyConnectDB.GetInstance().Page<CHI_TIET>(page.Value,6, "select * from CHI_TIET");
            return PartialView(dl);
        }
        //6 san ham ban chay
        // 6 san xem nhieu
        public ActionResult XEM_NHIEU(int id)
        {
            var dl = MyConnectDB.GetInstance().Page<CHI_TIET>(1, 6, "select * from CHI_TIET order by CHI_TIET.LUOT_XEM DESC");
            return View(dl);
        }
        // 3 san pham lien quan
        public ActionResult LIEN_QUAN(int id)
        {
            var db = MyConnectDB.GetInstance().Single<CHI_TIET>(id);
            var dl = MyConnectDB.GetInstance().Page<CHI_TIET>(1, 3, "SELECT * FROM CHI_TIET WHERE CHI_TIET.MA_LOAI=@0 ", db.MA_LOAI);
            return View(dl);
        } 
      

        //
        // GET: /Default1/Details/5

        public ActionResult Details(int id)
        {
            Session["tepm"] = id;
            var dl = MyConnectDB.GetInstance().Single<CHI_TIET>(id);
            dl.LUOT_XEM = dl.LUOT_XEM + 1;
            MyConnectDB.GetInstance().Update("CHI_TIET", "MA_SP",dl);
            var db = MyConnectDB.GetInstance().Single<CHI_TIET>(id);
            return View(db);
        }
        [HttpPost]
        public ActionResult PatialCreate(BAI_VIET baiviet)
        {
            try
            {
                baiviet.THOI_GIAN = DateTime.Now;

                MyConnectDB.GetInstance().Insert(baiviet);

                return RedirectToAction("Index");
            }
            catch
            {
                return RedirectToAction("Index");
            }
        }


        // danh sach loai san pham
        public ActionResult LoaiSP(int id)
        {
           var dl = MyConnectDB.GetInstance().Query<LOAI>("select * from LOAI ");
            return View(dl);
        }
        //sản phẩm theo loại
        public ActionResult XemLOAI(int id)
        {
            var dl = MyConnectDB.GetInstance().Query<CHI_TIET>("select * from CHI_TIET where CHI_TIET.MA_LOAI=@0", id);
            return View(dl);
        }
        // GET: /Default1/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /BAI_VIET/Create

        //
        // GET: /Default1/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Default1/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Default1/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Default1/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
