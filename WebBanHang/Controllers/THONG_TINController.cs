﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebBanHang.Controllers
{
    public class THONG_TINController : Controller
    {
        //
        // GET: /THONG_TIN/
        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /THONG_TIN/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /THONG_TIN/Create
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /THONG_TIN/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /THONG_TIN/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /THONG_TIN/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /THONG_TIN/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /THONG_TIN/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
