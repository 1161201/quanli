﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebBanHang.Controllers
{
    [Authorize(Roles = "Admin")]
    public class QUAN_LIController : Controller
    {
        //
        // GET: /QUAN_LI/
        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /QUAN_LI/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /QUAN_LI/Create
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /QUAN_LI/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /QUAN_LI/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /QUAN_LI/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /QUAN_LI/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /QUAN_LI/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
