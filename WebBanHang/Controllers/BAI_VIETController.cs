﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using System.Web.Configuration;
using MyConnect;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System.Threading.Tasks;
using PetaPoco;


namespace WebBanHang.Controllers
{
    public class BAI_VIETController : Controller
    {
        //
        // GET: /BAI_VIET/

        public ActionResult Index()
        {
            var dl = MyConnectDB.GetInstance().Query<BAI_VIET>("select * from BAI_VIET");
            return View(dl);
        }

        //
        // GET: /BAI_VIET/Details/5

        public ActionResult Details(int? page)
        {
            int id = (int)Session["tepm"];

            if (page == null)
                page = 1;
            Page<BAI_VIET> dl = MyConnectDB.GetInstance().Page<BAI_VIET>(page.Value, 10, "select * from BAI_VIET where BAI_VIET.MA_SP=@0 order by BAI_VIET.THOI_GIAN DESC", id);
            Session["MA"] = id;
            return PartialView("Details",dl);
        }

        //
        // GET: /BAI_VIET/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /BAI_VIET/Create
        [Authorize]
        [HttpPost]
        public ActionResult Create(BAI_VIET baiviet)
        {
            try
            {
                baiviet.TAC_GIA = User.Identity.GetUserName();
                baiviet.THOI_GIAN = DateTime.Now;
                baiviet.MA_SP = (int)Session["MA"];
                MyConnectDB.GetInstance().Insert(baiviet);

                return PartialView();
            }
            catch
            {
                return RedirectToAction("Index", "Home");
            }

        }
        //
        // GET: /BAI_VIET/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /BAI_VIET/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /BAI_VIET/Delete/5
         [Authorize(Roles = "Admin")]
        public ActionResult Delete(int id)
        {
            MyConnectDB.GetInstance().Delete<BAI_VIET>(id);
            return RedirectToAction("Index");
        }

        //
        // POST: /BAI_VIET/Delete/5

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return RedirectToAction("Index");
            }
        }
    }
}
