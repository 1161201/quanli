﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using System.Web.Configuration;
using MyConnect;

namespace WebBanHang.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        //tìm kiems cơ bản
        public ActionResult TIM_KIEM(string tukhoa)
        {
            try
            {
                var dl = MyConnectDB.GetInstance().Query<CHI_TIET>("select * from CHI_TIET where CHI_TIET.TEN_SP = @0", tukhoa);
                return View(dl);
            }
            catch
            {
                return RedirectToAction("Index");
            }
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}