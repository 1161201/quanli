﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebBanHang.Models;
using System.Data.SqlClient;
using System.Web.Configuration;
using MyConnect;
using PetaPoco;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System.Threading.Tasks;


namespace WebBanHang.Controllers
{
    public class CartController : Controller
    {
        //
        // GET: /Cart/
        [Authorize]
        public ActionResult Index()
        {
            string ng = User.Identity.GetUserName();
            var dl = MyConnectDB.GetInstance().Query<MyConnect.Cart>("select * from Cart where Cart.NguoiMua =@0",ng);
            return View(dl);
        }

        //
        
        //addcart
        public ActionResult AddCart(int id)
        {
            var temp = User.Identity.GetUserName();
            var nd = MyConnectDB.GetInstance().Query<NGUOI_DUNG>("select * from NGUOI_DUNG where NGUOI_DUNG.TEN_DN = @0",temp).FirstOrDefault();
            var db = MyConnectDB.GetInstance().Single<MyConnect.CHI_TIET>(id);
            var dulieu = new WebBanHang.Models.Cart();
            Session["SanPham"] = db.TEN_SP;
            Session["Gia"] = db.GIA;
            dulieu.NguoiMua = "null";
            dulieu.ID = 0;
            dulieu.SanPham = db.TEN_SP;
            dulieu.Tien = db.GIA;
            dulieu.SoLuong = 1;
            return View(dulieu);
        }
        // GET: /Cart/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Cart/Create
        public ActionResult Create()
        {
            return View();
        }

        //
     
        // POST: /Cart/Create
        [HttpPost]
        [Authorize]
        public ActionResult Create(MyConnect.Cart cart)
        {
            try
            {
                string ng = User.Identity.GetUserName();
                var nd = MyConnectDB.GetInstance().Query<NGUOI_DUNG>("select * from NGUOI_DUNG where NGUOI_DUNG.TEN_DN = @0", ng).FirstOrDefault();
                
                int mn = (int)Session["Gia"];
                cart.SDT = nd.SDT;
                cart.NGAY = DateTime.Now;
                cart.DIA_CHI = nd.DIA_CHI;
                cart.NguoiMua = ng;
                cart.Tien = cart.SoLuong * mn;
                cart.SanPham = (string)Session["SanPham"];
                MyConnectDB.GetInstance().Insert(cart);
                return RedirectToAction("Index");
            }
            catch
            {
                return RedirectToAction("Index");
            }
        }

        //
        // GET: /Cart/Edit/5
        [HttpPost]
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Cart/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Cart/Delete/5
        public ActionResult Delete(int id)
        {
            var dl=MyConnectDB.GetInstance().Single<MyConnect.Cart>(id);
            return View(dl);
        }

        //
        // POST: /Cart/Delete/5
        [HttpPost]
        [Authorize]
        public ActionResult Delete(int id, MyConnect.Cart nd)
        {
            try
            {
                // TODO: Add delete logic here
                var result = MyConnectDB.GetInstance().Delete(nd);
                return RedirectToAction("Index");
            }
            catch
            {
                return RedirectToAction("Index");
            }
        }
    }
}
