﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebBanHang.Models;
using System.Data.SqlClient;
using System.Web.Configuration;
using MyConnect;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System.Threading.Tasks;

namespace WebBanHang.Controllers
{
    public class NGUOI_DUNGController : Controller
    {
        public UserManager<ApplicationUser> UserManager { get; private set; }
        public RoleManager<IdentityRole> RoleManager { get; private set; }
        //
        // GET: /NGUOI_DUNG/
        [Authorize(Roles="Admin")]
        public ActionResult Index()
        {
            var dl = MyConnectDB.GetInstance().Query<NGUOI_DUNG>("select * from NGUOI_DUNG");
            return View(dl);
        }

        //
        // GET: /NGUOI_DUNG/Details/5
        [Authorize(Roles = "Admin")]
        public ActionResult Details()
        {
            var dl = MyConnectDB.GetInstance().FirstOrDefault<NGUOI_DUNG>("select * from NGUOI_DUNG where TEN_DN=@0", @User.Identity.Name);
            return View(dl);
        }
        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }
        public NGUOI_DUNGController()
        {
            UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext()));
 
        }
        //carl
   
        // GET: /NGUOI_DUNG/Create

        public ActionResult Create()
        {
            return View();
        }

        // POST: /NGUOI_DUNG/Create

        [HttpPost]
        public async Task<ActionResult> Create(NGUOI_DUNG nguoidung)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var user = new ApplicationUser() { UserName = nguoidung.TEN_DN };
                    var result = await UserManager.CreateAsync(user, nguoidung.PASS);
                    if (result.Succeeded)
                    {

                        await UserManager.AddToRoleAsync(user.Id, "User");
                        MyConnectDB.GetInstance().Insert(nguoidung);
                        Session["UserName"] = nguoidung.TEN_DN;
                        await SignInAsync(user, isPersistent: false);
                        return RedirectToAction("DangNhap");
                    }
                    else
                    {
                        ModelState.AddModelError("", "Tên đăng nhập đã tồn tại");
                    }
                }
                catch
                {
                    return RedirectToAction("Index", "Home");
                }


            }
            return View();
        }

        //cap nhat thong tin
        [Authorize]
        public ActionResult CapNhat(string id)
        {
            var i = User.Identity.GetUserName();
            var dl = MyConnectDB.GetInstance().Query<NGUOI_DUNG>("select * from NGUOI_DUNG where NGUOI_DUNG.TEN_DN=@0",i).FirstOrDefault();

            return View(dl);
        }

        //
        // POST: /NGUOI_DUNG/Edit/5
        [Authorize]
        [HttpPost]
        public ActionResult CapNhat(int id, NGUOI_DUNG nguoidung)
        {
            try
            {
                var dl = MyConnectDB.GetInstance().Update(nguoidung);

                return RedirectToAction("Index","Home");
            }
            catch
            {
                return RedirectToAction("Index","Home");
            }
        }
        // GET: /NGUOI_DUNG/Edit/5
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int id)
        {
            var dl = MyConnectDB.GetInstance().Single<NGUOI_DUNG>(id);

            return View(dl);
        }

        //
        // POST: /NGUOI_DUNG/Edit/5
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Edit(int id, NGUOI_DUNG nguoidung)
        {
            try
            {
                var dl = MyConnectDB.GetInstance().Update(nguoidung);

                return RedirectToAction("Index");
            }
            catch
            {
                return RedirectToAction("Index");
            }
        }
  
        // GET: /NGUOI_DUNG/Delete/5
         [Authorize(Roles = "Admin")]
        public ActionResult Delete(int id)
        {
            var dl = MyConnectDB.GetInstance().Single<NGUOI_DUNG>(id);
            if (dl.TEN_DN == User.Identity.GetUserName())

               return RedirectToAction("Index");
            
            return View(dl);

        }

        //
        // POST: /NGUOI_DUNG/Delete/5
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Delete(int id, NGUOI_DUNG nd)
        {
            try
            {
                if (nd.TEN_DN == User.Identity.GetUserName())
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    // TODO: Add delete logic here
                        var result = MyConnectDB.GetInstance().Delete<NGUOI_DUNG>(nd);
                        return RedirectToAction("Index");
                }
            }
            catch
            {
                return RedirectToAction("Index");
            }
        }
        [AllowAnonymous]
        public ActionResult DangNhap(string returnUrl)
        {
            if (Session["UserName"] != null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                ViewBag.ReturnUrl = returnUrl;
                return View();
            }

        }
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DangNhap(NGUOI_DUNG u)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindAsync(u.TEN_DN, u.PASS);
                if (user != null)
                {
                    await SignInAsync(user, isPersistent: true);
                    Session["UserName"] = u.TEN_DN;
                    return Redirect("/");
                }
                else
                {
                    ModelState.AddModelError("", "Tên đăng nhập hoặc mật khẩu không chính xác");
                }
            }

            // If we got this far, something failed, redisplay form
            return View();
        }

        private async Task SignInAsync(ApplicationUser user, bool isPersistent)
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            var identity = await UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = isPersistent }, identity);
        }
	}
}