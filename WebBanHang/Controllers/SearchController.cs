﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MyConnect;

namespace WebBanHang.Controllers
{
    public class SearchController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult result(FormCollection f)
        {
            string tukhoa = f["search"].ToString();
            var result = MyConnectDB.GetInstance().Query<CHI_TIET>("select * from [CHI_TIET] where Name like '%" + tukhoa + "%'");
            //db.

            return View(result);
            //
        }

        [HttpPost]
        public ActionResult timkiemmorong(FormCollection f)
        {
            string ten = f["searchname"].ToString();
            string loai = f["searchcategory"].ToString();
            string mau = f["searchcolor"].ToString();
            string giakieustring = f["pricetu"].ToString();
            float giatu = float.Parse(giakieustring);
            string giakieustring1 = f["priceden"].ToString();
            float giaden = float.Parse(giakieustring1);

            if (string.IsNullOrEmpty(ten)) //ten null   
            {
                if (string.IsNullOrEmpty(loai)) //loai null
                {
                    if (string.IsNullOrEmpty(mau)) // null
                    {
                        var result = MyConnectDB.GetInstance().Query<CHI_TIET>("select * from [CHI_TIET] where PriceExport between " + giatu + " and " + giaden + "");
                        return View(result);
                    }
                    else //mau k null
                    {
                        var result = MyConnectDB.GetInstance().Query<CHI_TIET>("select * from [CHI_TIET] where Color like N'%" + mau + "%' and PriceExport between " + giatu + " and " + giaden + "");
                        return View(result);
                    }
                }

                else  // loai k null
                {
                    if (string.IsNullOrEmpty(mau)) //mau null
                    {
                        var result = MyConnectDB.GetInstance().Query<CHI_TIET>("select * from [CHI_TIET] where CategoryID like '%" + loai + "%' and PriceExport between " + giatu + " and " + giaden + "");
                        return View(result);
                    }
                    else // mau k null
                    {
                        var result = MyConnectDB.GetInstance().Query<CHI_TIET>("select * from [CHI_TIET] where CategoryID like '%" + loai + "%' and Color like N'%" + mau + "%' and PriceExport between " + giatu + " and " + giaden + "");
                        return View(result);
                    }
                }
            }
            else // ten  k null
            {
                if (string.IsNullOrEmpty(loai)) // loai null
                {
                    if (string.IsNullOrEmpty(mau)) //mau null
                    {
                        var result = MyConnectDB.GetInstance().Query<CHI_TIET>("select * from [CHI_TIET] where Name like '%" + ten + "%' and PriceExport between " + giatu + " and " + giaden + "");
                        return View(result);
                    }
                    else // mau k null
                    {
                        var result = MyConnectDB.GetInstance().Query<CHI_TIET>("select * from [CHI_TIET] where Color like N'%" + mau + "%' and Name like '%" + ten + "%' and PriceExport between " + giatu + " and " + giaden + "");
                        return View(result);
                    }
                }
                else // loai k null
                {
                    if (string.IsNullOrEmpty(mau)) //mau null
                    {
                        var result = MyConnectDB.GetInstance().Query<CHI_TIET>("select * from [CHI_TIET] where CategoryID like '%" + loai + "%' and Name like '%" + ten + "%' and PriceExport between " + giatu + " and " + giaden + "");
                        return View(result);
                    }
                    else // mau k null
                    {
                        var result = MyConnectDB.GetInstance().Query<CHI_TIET>("select * from [CHI_TIET] where Color like N'%" + mau + "%' and CategoryID like '%" + loai + "%' and Name like '%" + ten + "%' and PriceExport between " + giatu + " and " + giaden + "");
                        return View(result);
                    }
                }
            }
        }

        //
        // GET: /Search/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }
    }
}
