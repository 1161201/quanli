﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebBanHang.Controllers
{
    public class DICH_VUController : Controller
    {
        //
        // GET: /DICH_VU/
        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /DICH_VU/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /DICH_VU/Create
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /DICH_VU/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /DICH_VU/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /DICH_VU/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /DICH_VU/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /DICH_VU/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
