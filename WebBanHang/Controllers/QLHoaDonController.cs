﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using System.Web.Configuration;
using MyConnect;
namespace WebBanHang.Controllers
{
    [Authorize(Roles = "Admin")]
    public class QLHoaDonController : Controller
    {
        //
        // GET: /QLHoaDon/
        public ActionResult Index()
        {
            var dl = MyConnectDB.GetInstance().Query<MyConnect.Cart>("select * from Cart");
            return View(dl);
        }

        //
        // GET: /QLHoaDon/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /QLHoaDon/Create
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /QLHoaDon/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /QLHoaDon/Edit/5
        public ActionResult Edit(int id)
        {
            var dl = MyConnectDB.GetInstance().Single<Cart>(id);
            return View(dl);
        }

        //
        // POST: /QLHoaDon/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Cart cart)
        {
            try
            {
                var dl = MyConnectDB.GetInstance().Query<CHI_TIET>("select * from CHI_TIET where CHI_TIET.TEN_SP=@0",cart.SanPham).FirstOrDefault();
                cart.Tien = dl.GIA * cart.SoLuong;
                cart.NGAY = DateTime.Now;
                MyConnectDB.GetInstance().Update(cart);

                return RedirectToAction("Index");
            }
            catch
            {
                return RedirectToAction("Index");
            }
        }

        //
        // GET: /QLHoaDon/Delete/5
        public ActionResult Delete(int id)
        {
            MyConnectDB.GetInstance().Delete<Cart>(id);
            return RedirectToAction("Index");
        }

        //
        // POST: /QLHoaDon/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
