﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using System.Web.Configuration;
using MyConnect;
using PetaPoco;
using System.IO;

namespace WebBanHang.Controllers
{
    [Authorize(Roles = "Admin")]
    public class QLSanPhamController : Controller
    {
        //
        // GET: /QLSanPham/
        public ActionResult Index()
        {
            var db = MyConnectDB.GetInstance().Query<CHI_TIET>("select * from CHI_TIET");
            return View(db);
        }
        public ActionResult BAN_NHIEU()
        {

            var dl = MyConnectDB.GetInstance().Page<CHI_TIET>(1, 10, "select * from CHI_TIET order by CHI_TIET.LUOT_MUA DESC");
            return View(dl);
        }

        //
        // GET: /QLSanPham/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /QLSanPham/Create
        public ActionResult Create()
        {
            return View();
        }

        // json uoload
        public ActionResult Upload()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase File)
        {
            if (File != null)
            {
                if (File.ContentLength != 0)
                {
                    string path = Server.MapPath("~") + "Images";
                    string file = Request.Files[0].FileName;
                    Request.Files[0].SaveAs(path + "\\" + file);
                    Session["filename"] = file;
                    File.SaveAs(path + file);
                }
            }
             return RedirectToAction("Create");
           

        }
        // POST: /QLSanPham/Create
        [HttpPost]
        public ActionResult Create(CHI_TIET chitiet)
        {
            try
            {
                // TODO: Add insert logic here
                chitiet.HINH = (string)Session["filename"];
                MyConnectDB.GetInstance().Insert(chitiet);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /QLSanPham/Edit/5
        public ActionResult Edit(int id)
        {
            var dl = MyConnectDB.GetInstance().Single<CHI_TIET>(id);
            return View(dl);
        }

        //
        // POST: /QLSanPham/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, CHI_TIET chitiet)
        {
            try
            {
                if (Session["filename"] != null)
                {
                    chitiet.HINH = chitiet.HINH = (string)Session["filename"];
                }
                    MyConnectDB.GetInstance().Update(chitiet);
                    return RedirectToAction("Index");
                
            }
            catch
            {
                return RedirectToAction("Index");
            }
        }

        //
        // GET: /QLSanPham/Delete/5
        public ActionResult Delete(int id)
        {
            MyConnectDB.GetInstance().Delete<CHI_TIET>(id);
             return RedirectToAction("Index");
            
        }

        //
        // POST: /QLSanPham/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, CHI_TIET sd)
        {
            try
            {

               // TODO: Add delete logic here
                return RedirectToAction("Index");
            }
            
            catch
            {
                return View();
            }
        }
    }
}
