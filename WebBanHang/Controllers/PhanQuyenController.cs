﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using System.Web.Configuration;
using MyConnect;
using PetaPoco;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System.Threading.Tasks;

namespace WebBanHang.Controllers
{
    
    public class PhanQuyenController : Controller
    {
        //
        // GET: /PhanQuyen/
        [Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
            var dl = MyConnectDB.GetInstance().Query<AspNetUser>("select * from AspNetUsers");
            return View(dl);
        }

        //
        // GET: /PhanQuyen/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /PhanQuyen/Create
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /PhanQuyen/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //

        // GET: /PhanQuyen/Edit/5
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(string id)
        {
            var b = MyConnectDB.GetInstance().Query<AspNetUserRole>("select * from AspNetUserRoles where AspNetUserRoles.UserId=@0", id).FirstOrDefault();
            return View(b);
        }

        //
        // POST: /PhanQuyen/Edit/5
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Edit(string id, AspNetUserRole nd)
        {
            try
            {
                // TODO: Add update logic here
                var dl = MyConnectDB.GetInstance().Update(nd);
                return RedirectToAction("Index");
            }
            catch
            {
                return RedirectToAction("Index");
            }
        }

        //
        // GET: /PhanQuyen/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(string id)
        {
            
            var dl = MyConnectDB.GetInstance().Query<AspNetUser>("select * from AspNetUsers where AspNetUsers.Id=@0 ",id).FirstOrDefault();
            if (dl.UserName==User.Identity.GetUserName())
                return RedirectToAction("Index");
             return View(dl);
            

        }


        //
        // POST: /NGUOI_DUNG/Delete/5
         [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Delete(string id, AspNetUser nd)
        {
            try
            {
                var dl = MyConnectDB.GetInstance().Delete<AspNetUser>(nd);
                return RedirectToAction("Index");

            }
            catch
            {
                return RedirectToAction("Index");
            }
        }
    }
}
            
        
