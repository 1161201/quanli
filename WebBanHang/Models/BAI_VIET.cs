﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebBanHang.Models
{
    public class BAI_VIET
    {

        public int MA_BAI { get; set; }

        public string TIEU_DE { get; set; }

        public string TAC_GIA { get; set; }

        public DateTime THOI_GIAN { get; set; }
        [AllowHtml]
        public string NOI_DUNG { get; set; }
    }
}