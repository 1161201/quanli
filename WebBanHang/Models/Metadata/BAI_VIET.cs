﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebBanHang.Models.Metadata
{
    [MetadataType(typeof(BAI_VIETMetadata))]
    public partial class BAI_VIET
    {
        private class BAI_VIETMetadata
        {
            
            [Display(Name="Nội dung")]
            public string NOI_DUNG;
        }
    }
}