﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebBanHang.Models
{
    public class CHI_TIET
    {
        public int MA_SP { get; set; }
        public int MA_LOAI { get; set; }
        public int GIA { get; set; }
        public int LUOT_XEM { get; set; }
        public String TEN_SP { get; set; }
        public int LUOT_MUA { get; set; }
        public String HINH { get; set; }
        public String MO_TA { get; set; }
    }
}